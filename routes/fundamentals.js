const express = require('express');
const router = express.Router();

require('dotenv').config()
const variableData = process.env.variableData || 'Fundamentals'

router.get('/', function(req, res, next) {
  res.send({
    name: 'Fundamentals',
    server: 'express',
    variableData: variableData
  });
});

router.get('/sumTwonumbers', function(req, res, next) {
  res.send({
    name: 'sumTwonumbers',
    sum: sumTwonumbers(1,2)
  });
  });
function sumTwonumbers(a, b)
{
    return a +b;
}
module.exports = router;
